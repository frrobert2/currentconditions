# currentconditions
Short one line bash script the gets the current weather conditions for a given location.

The current location the South Bend, IN Airport. To change location replace KSBN with your local National Weather Service location.

It is designed to be an add on script for the Chameleon conky script.

It requires curl and jq to run.
